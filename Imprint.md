
Codeberg is a non-profit organisation dedicated to build and maintain supporting infrastructure for the creation, collection, dissemination, and archiving of Free and Open Source Software. If you have questions, suggestions or comments, please do not hesitate to contact us at [contact@codeberg.org](mailto:contact@codeberg.org).

## Impressum nach §5 TMG (Imprint according to German Law)

```text
Codeberg e.V.
Gormannstraße 14
10119 Berlin

E-Mail: contact@codeberg.org

Geschäftsführender Vorstand: Holger Waechtler
Eingetragen im Vereinsregister des Amtsgerichts Charlottenburg VR36929.
```

